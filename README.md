# app
Website and Phonegap App

## Dev Env Instructions
```
sudo npm install -g gulp
npm i
gulp
```

Now make sure you have `node app.js` running with the specified directory structure, and visit `localhost:3000` and you should see the app.

## Gulp Info

We're using `gulp` to package the code from `src/` to `www/`. By default, just typing:
```
gulp
```

is equivalent to typing:
```
gulp build && gulp watch
```

Sometimes, `gulp watch` (the command that watches for changes and allows you to do a live refresh) will hangup and not register your changes – you can easily see that because no new terminal commands will appear when you save. 

If that happens, just run `gulp` again.