const common = (a, errs) => {
  return errs;
};

const validators = {
  mc: (a, errs) => {
    // if (!(a.maxSelect && a.maxSelect >= 1))
      // errs.push({maxSelect: 'Must be defined and a natural number.'});
    return errs;
  },
  dd: (a, errs) => {
    // if (!(a.maxSelect && a.maxSelect >= 1))
      // errs.push({maxSelect: 'Must be defined and a natural number.'});
    return errs;
  },
  slider: (a, errs) => errs
};

exports.validateAnswer = function(a, errors = []) {
  return validators[a.type](a, common(a, errors));
};