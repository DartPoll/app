const common = (s, errs) => {
  return errs;
};

const validators = {
  mc: (s, errs) => {
    if (!(s.maxSelect && s.maxSelect >= 1))
      errs.push({maxSelect: 'Must be defined and a natural number.'});
    return errs;
  },
  dd: (s, errs) => {
    if (!(s.maxSelect && s.maxSelect >= 1))
      errs.push({maxSelect: 'Must be defined and a natural number.'});
    return errs;
  },
  slider: (s, errs) => errs
};

exports.validateSurvey = function(s) {
  let errors = [];
  return validators[s.type](s, common(s, errors));
};