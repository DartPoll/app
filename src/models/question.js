const common = (q, errs) => {
  return errs;
};

const validators = {
  mc: (q, errs) => {
    if (!(q.maxSelect && q.maxSelect >= 1))
      errs.push({maxSelect: 'Must be defined and a natural number.'});
    return errs;
  },
  dd: (q, errs) => {
    if (!(q.maxSelect && q.maxSelect >= 1))
      errs.push({maxSelect: 'Must be defined and a natural number.'});
    return errs;
  },
  slider: (q, errs) => errs
};

exports.validateQuestion = function(q) {
  let errors = [];
  return validators[q.type](q, common(q, errors));
};