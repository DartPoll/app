/**
 * 
 */

class Attribute {
  constructor(name, options, multiSelect) {
    this.name = name;
    this.options = options;
    this.multiSelect = multiSelect;
    this.active = [];
  }

  toggle(active) {
    var idx = this.active.indexOf(active);

    if (idx > -1) {
      this.active.splice(idx, 1);
    }

    else {
      this.active.push(active);
    }
  }

  match(answers) {
    if (this.active.length == 0)
      return answers;

    return answers.filter(a => this.active.indexOf(a.demographics[this.name]) > -1);
  }

  clear() {
    this.active = [];
  }

  stringify() {
    return this.active.join(',');
  }
}

/**
 * Helper class to manage demographics and filtering of answers
 */

class DemographicsFilter {
  constructor() {
    this.attributes = {
      year: new Attribute('year', ['12', '13', '14', '15', '16', '17', '18', '19', '20', 'GR'], false),
      gender: new Attribute('gender', ['male', 'female', 'transgender', 'genderqueer'], true),
      race: new Attribute('race', ['native', 'asian', 'black', 'latino', 'white', 'multiple', 'other'], true),
      affiliation: new Attribute('affiliation', ['fraternity', 'sorority', 'coed', 'none'], false),
      athlete: new Attribute('athlete', ['yes', 'no'], false),
      religion: new Attribute('religion', ['none', 'atheist', '...]'], true)
    };
  }

  toggle(attr, value) {
    this.attributes[attr].toggle(value);
  }

  filter(answers) {
    var results = answers.slice();
    for (var attr in this.attributes) {
      results = this.attributes[attr].match(results);
    }
    return results;
  }

  attributeStrings() {
    return Object.keys(this.attributes);
  }

  clear() {
    for (var attr in this.attributes)
      this.attributes[attr].clear();
  }

  activeFor(attr) {
    return this.attributes[attr].active;
  }

  optionsFor(attr) {
    return this.attributes[attr].options;
  }

  stringify() {
    var children = this.attributeStrings().map(a => this.attributes[a].stringify());
    var talkativeChildren = children.filter(c => c != '');
    var result =  talkativeChildren.join(',');
    if (result != '')
      return result;

    return null;
  }
}

export default new DemographicsFilter();
