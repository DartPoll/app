export default function (values, possibleValues) {
  var valToIdx = {};

  possibleValues.forEach((val, idx) => {
    valToIdx[val] = idx;
  });

  return values.map(v => valToIdx[v]);
};