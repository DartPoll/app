export default function (answers) {
  var values = {};
  var selections = [];

  answers.forEach(a => {
    a.selections.forEach(s => {
      if (!values[s]) values[s] = 0;

      values[s] += 1;
      selections.push(s);
    });
  });

  return {values, selections};
};