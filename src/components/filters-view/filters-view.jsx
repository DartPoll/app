import React from 'react';
import Hammer from 'react-hammerjs';
import closest from 'component-closest';
import dom from 'component-dom';

export default class FiltersView extends React.Component {
  constructor() {
    super();
    this.state = {
      collapsed: true,
      activeCategory: null
    };
  }

  componentWillMount() {
    this.categories = this.props.DemographicsFilter.attributeStrings();
    this.state.activeCategory = this.categories[0];
  }

  onCatClick(ev) {
    let clicked = dom(closest(ev.target, '.category', true)).attr('data');

    if (clicked == 'clearall')
      return this.clearAll();

    this.setState({
      activeCategory: clicked
    });
  }

  onOptClick(ev) {
    let clicked = dom(closest(ev.target, '.option', true)).attr('data');
    this.props.DemographicsFilter.toggle(this.state.activeCategory, clicked);
    this.forceUpdate();
  }

  clearAll() {
    this.props.DemographicsFilter.clear();
    this.forceUpdate();
  }

  toggleCollapsed(ev) {
    ev.srcEvent.stopPropagation();
    this.setState({
      collapsed: !this.state.collapsed
    });
    if (this.state.collapsed)
      this.props.onFiltersChange();
  }

  render() {
    let createCat = (cat, text) => {
      let catTextClass = (this.state.activeCategory == cat) ? 'selected' : '';
      return (
        <Hammer onTap={this.onCatClick.bind(this)} key={cat}>
          <div className="category" data={cat}>
            <div className={`category-text ${catTextClass} ${cat}`}>
              <span>{text}</span>
            </div>
          </div>
        </Hammer>
    )};

    let headerText = this.props.DemographicsFilter.stringify();
    if (!headerText)
      headerText = 'Filters';

    let headerClass = this.state.collapsed ? 'collapsed' : 'expanded';
    let collapsedContents, expandedContents, saveButton;

    if (this.state.collapsed) {
      collapsedContents = (
        <Hammer onTap={this.toggleCollapsed.bind(this)}>
          <div className={`filters-header ${headerClass}`}>
            <div className="filters-header-text">
              <span>{headerText}</span>
            </div>
          </div>
        </Hammer>
      );      
    }

    if (!this.state.collapsed) {
      let categories = this.categories.map(c => createCat(c, c));
      categories.push(createCat('clearall', 'Clear All'));

      let options = this.props.DemographicsFilter.optionsFor(this.state.activeCategory).map(o => {

        let selected = this.props.DemographicsFilter.activeFor(this.state.activeCategory).indexOf(o) > -1;
        let selectedBox;
        if (selected)
          selectedBox = ( <div className="option-selected"></div> );

        return (
          <Hammer onTap={this.onOptClick.bind(this)} key={o}>
            <div className="option" data={o}>
              <div className="option-box">
                {selectedBox}
              </div>
              <div className="option-text">
                <span>{o}</span>
              </div>
            </div>
          </Hammer>
      )});

      expandedContents = (
        <div className="filters-container">
          <div className="categories-container">
            {categories}
          </div>
          <div className="options-container">
            {options}
          </div>
        </div>
      );

      saveButton = (
        <Hammer onTap={this.toggleCollapsed.bind(this)}>
          <div className="save-button">
            <div className="save-button-text">
              <span>Save</span>
            </div>
          </div>
        </Hammer>
      );
    }

    return (
      <div className="filters-view">
        {collapsedContents}
        {expandedContents}
        {saveButton}
      </div>
    );
  }
};
