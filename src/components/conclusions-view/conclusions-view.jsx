import React from 'react';
import SurveyCard from '../survey-card/survey-card';
import QBox from '../q-box/q-box';
import Hammer from 'react-hammerjs';
import api from '../../api';

export default class ConclusionsView extends React.Component {
  constructor() {
    super();
    this.state = {
      idx: 0
    };
  }

  onPan(ev) {
    switch (ev.direction) {
      // pan towards the right
      case 2:
        this.setState({
          idx: Math.min(this.state.idx + 1, this.props.conclusions.length - 1)
        });
        break;

      // pan towards the left
      case 4:
        this.setState({
          idx: Math.max(this.state.idx - 1, 0)
        });
        break;
    }
  }

  render() {
    let panOptions = {
      direction: ['left', 'right'],
      threshold: 10
    };

    let dots = this.props.conclusions.map((c, i) => {
      let cn = (i == this.state.idx) ? 'selected' : '';
      return (
        <div className={`dot ${cn}`} key={i}> </div>
      );
    });

    return (
      <Hammer onPanEnd={this.onPan.bind(this)} options={panOptions}>
        
        <div className="conclusions-view">
          <div className="conclusion-text">
            <span>{this.props.conclusions[this.state.idx]}</span>
          </div>

          <div className="conclusion-dots">
            {dots}
          </div>
        </div>

      </Hammer>
    );
  }
};
