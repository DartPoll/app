import React from 'react';

export default class QBox extends React.Component {
  constructor() {
    super();
  }

  render() {
    let indexes = [];
    let idx = 0;

    let segments = [];

    while (idx < this.props.max) {
      if (idx == this.props.current) {
        segments.push((
          <div className="current" key={`${name}-${idx}`}>
            <div className="before-thing"></div>
            <div className="middle-thing"></div>
            <div className="after-thing"></div>
          </div>
        ));
      }

      else if (idx < this.props.current) {
        segments.push((<div className="before" key={`${name}-${idx}`}> </div>));
      }

      else if (idx > this.props.current) {
        segments.push((<div className="after" key={`${name}-${idx}`}> </div>));
      }

      idx++;
    }

    return (
      <div className="progress-bar">
        {segments}
      </div>
    );
  }
};
