import React from 'react';
import closest from 'component-closest';
import dom from 'component-dom';
import Hammer from 'react-hammerjs';

export default class MC extends React.Component {
  constructor() {
    super();
  }

  onOptionClick(ev) {
    let selection = dom(closest(ev.target, '.mc-option', true)).attr('data');
    this.props.addSelection(selection);
  }

  render() {
    let options = this.props.question.options.map(o => {
      let selectedComponent = (this.props.selections.indexOf(o.value) > -1) ? (<div className="mc-option-selected"></div>) : null;
      return (
        <Hammer onTap={this.onOptionClick.bind(this)} key={o.value}>
          <div className="mc-option" key={o.value} data={o.value} >
            <div className="mc-option-box">
              {selectedComponent}
            </div>
            <div className="mc-option-text">
              <div className="mc-option-text-container">
                <span data={o.value}>{o.text}</span>
              </div>
            </div>
          </div>
        </Hammer>
      );
    });

    return (
      <div className="mc-question">
        {options}
      </div>
    );
  }
};
