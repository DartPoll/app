import React from 'react';
import ProgressBar from '../progress-bar/progress-bar';
import mcQuestionType from './types/mc-question';

const questionTypeComponents = {
  mc: mcQuestionType
};

module.exports = class QBox extends React.Component {
  constructor() {
    super();
  }

  addSelection(selection) {
    let newSelections = this.props.answer.selections;

    if (this.props.question.maxSelect == 1) {
      newSelections = [selection]
    }
    
    else {
      let currentPlace = this.props.answer.selections.indexOf(selection);
      if (currentPlace > -1) {
        newSelections.splice(currentPlace, 1);
      }

      else {
        if (this.props.question.maxSelect > newSelections.length)
          newSelections = newSelections.concat([selection]);
      }
    }

    this.props.setAnswer({
      question: this.props.question.id,
      selections: newSelections,
      type: this.props.question.type
    });
  }

  render() {
    let Type = this.props.question ? questionTypeComponents[this.props.question.type] : null;

    let qExists = [];
    let qNotExist;

    if (this.props.question) {
      qExists.push((
        <ProgressBar max={this.props.survey.questions.length} key="progress-bar"
          current={this.props.survey.questions.indexOf(this.props.question)}
          name="qbox" />
      ));

      qExists.push((
        <div className="question-container" key="question-container">
          <div className="question-header">

            <div className="number-container">
              <div className="number">
                <span>{`Q${this.props.survey.questions.indexOf(this.props.question) + 1}`}</span>
              </div>
              <div className="out-of">
                <span>{`of ${this.props.survey.questions.length}`}</span>
              </div>
            </div>

            <div className="question-text-container">
              <div className="question-text">
                <span>{this.props.question.description}</span>
              </div>
            </div>

          </div>
        </div>
      ));

      qExists.push((
        <div className="answer-container" key="answer-container">
          <Type selections={this.props.answer.selections} 
            question={this.props.question} 
            addSelection={this.addSelection.bind(this)} />
        </div>
      ));
    }

    else {
      qNotExist = (
        <div className="survey-complete-view">
          <div className="survey-complete-icon">
            <img src="img/bank-icon-white.svg" />
          </div>
          <div className="survey-complete-text">
            <span>{this.props.submittingInProgress ? 'Loading...' : `${this.props.survey.pointValue} reward points have been added to your balance!`}</span>
          </div>
        </div>
      );
    }

    return (
      <div className="q-box-container">
        {qExists}
        {qNotExist}
      </div>
    );
  }
};
