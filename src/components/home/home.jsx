import React from 'react';
import SurveyCard from '../survey-card/survey-card';
import Hammer from 'react-hammerjs';
import closest from 'component-closest';
import dom from 'component-dom';

export default class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      ongoing: true
    };
  }

  onFilterClick(ev) {
    var chosen = dom(closest(ev.target, '.filter-opt', true)).attr('data');
    this.setState({
      ongoing: chosen == 'ongoing'
    });
  }

  /**
   * Controls what happens on a home swipe 
   */
  onHomePan(ev) {
    let shouldStopPropagation = false;
    switch (ev.direction) {
      // pan towards the right
      case 2:
        if (this.state.ongoing) {
          this.setState({ongoing: false});
          shouldStopPropagation = true;
        }
        break;

      // pan towards the left
      case 4:
        if (!this.state.ongoing) {
          this.setState({ongoing: true});
          shouldStopPropagation = true;
        }
        break;
    }

    if (shouldStopPropagation)
      ev.srcEvent.stopPropagation();
  }

  render() {
    let surveys = this.props.surveys.filter(s => s.complete == !this.state.ongoing);

    // counts of completed and non-completed surveys
    let otherSurveys = this.props.surveys.filter(s => s.complete == this.state.ongoing)
    let ongoingCount = (this.state.ongoing ? surveys.length : otherSurveys.length)
    let completeCount = (!this.state.ongoing ? surveys.length : otherSurveys.length)

    let panOptions = {
      direction: ['left', 'right'],
      threshold: 20
    };

    let surveyCards = [];
    surveys.forEach(s => {
      surveyCards.push((<div className="boundary-div" key={`boundary-div-${s.id}`}> </div>));
      surveyCards.push((<SurveyCard survey={s} key={s.id} setActive={this.props.setActive} />));
    });

    return (
      <div className="home-container">
        <div className="filter-container">
          <Hammer onTap={this.onFilterClick.bind(this)}>
            <div className={`filter-opt ${this.state.ongoing ? 'selected': ''}`} data="ongoing">
              <div className="filter-text-container">
                <span>{'Ongoing (' + ongoingCount + ")"}</span>
              </div>
            </div>
          </Hammer>

          <Hammer onTap={this.onFilterClick.bind(this)}>
            <div className={`filter-opt ${!this.state.ongoing ? 'selected': ''}`} data="completed">
              <div className="filter-text-container">
                <span>{'Complete (' + completeCount + ")"}</span>
              </div>
            </div>
          </Hammer>
        </div>

        <Hammer onPanEnd={this.onHomePan.bind(this)} options={panOptions}>
          <div className="cards-container">
            {surveyCards}
            <div className="padding-div"></div>
          </div>
        </Hammer>
      </div>
    );
  }
};
