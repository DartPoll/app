import React from 'react';
import Hammer from 'react-hammerjs';
import api from '../../api';
import store from 'store';

export default class PrizeCard extends React.Component {
  constructor() {
    super();
    this.state = {
      claiming: false,
      claimed: false
    };
  }

  onClick() {
    if (!this.state.claiming) {
      this.setState({claiming: true});      
    } else {
      this.setState({claiming: false});
    }
  }

  onClickClaim() {
    debugger;
    let user = store.get('user');
    if (user.numPoints >= this.props.prize.cost) {
      api.prize.claim(this.props.prize.id)
      .then(user => {
        this.props.resetUser(user);
        this.setState({claimed: true});
      })
      .catch(err => {
        alert(err);
      })
    } else {

    }
  }

  render() {
    // access prize props through this.props.prize.whatever
    // if you're supposed to show that claim thing, do it

    let prizeTitle = this.props.prize.title
    let claimOverlay;
    if (this.state.claiming) {
      claimOverlay = (
        <div className="overlay">
          <Hammer onTap={this.onClickClaim.bind(this)}>
            <div className="claim-box">
              <span>CLAIM</span>
            </div>
          </Hammer>
          
        </div>
      );
    } 

    return (
      <Hammer onTap={this.onClick.bind(this)}>
        <div className="prize-card" style={{paddingLeft: '10px', paddingRight: '10px'}}>
          {claimOverlay}
          <div className="banner-container" style={{backgroundImage:`url(${this.props.prize.imgSrc})`}} >
            <div className="prize-title-container">
              <div className="prize-title">
                <span>{prizeTitle}</span>
              </div>
            </div>
          </div>
          <div className="bottom-container">
            <div className="description-container">
              <span>{this.props.prize.description}</span>
            </div> 
            <div className="right-box">
              <div className="pointvalue-container">
                <span>{this.props.prize.cost}</span>
              </div>
              <div className="pointtext-container">
                <span>Reward Points</span>
              </div>
            </div>
          </div>
        </div>
      </Hammer>
    );
  }
};
