import React from 'react';
import Hammer from 'react-hammerjs';
import PrizeCard from '../prize-card/prize-card';
import api from '../../api';
import store from 'store';

export default class ConclusionsView extends React.Component {
  constructor() {
    super();
    this.state = {
      data: null
    };
  }

  componentWillMount() {
    api.prize.all()
    .then(prizes => {
      this.setState({data: prizes});
    })
    .catch(err => {
      alert(err);
    });
  }

  componentWillUnmount() {
    this.props.resetUser();
  }


  render() {
    let prizes = [];
    if (this.state.data) {
      this.state.data.forEach(p => {
        prizes.push((<div className="boundary-div" key={`boundary-div-${p.id}`} ></div>));
        prizes.push((<PrizeCard resetUser={this.props.resetUser} prize={p} key={p.id} />));
      });
    }

    return (
      <div className="bank-container">
        <div className="prizes-container">
          {prizes}
          <div className="padding-div"></div>
        </div>
      </div>
    );
  }
};
