import React from 'react';
import Hammer from 'react-hammerjs';
import store from 'store';

export default class Header extends React.Component {
  constructor() {
    super();
  }
  
  render() {
    let user = store.get('user');

    return (
      <div className="header">

        <Hammer onTap={this.props.toggleSidebar}>
          <div className="logo-container">
            <img src="img/PollU_FGLogo.png"/>
          </div>
        </Hammer>

        <div className="points-container">
          <div className="points-box">
            <span> {user.numPoints} </span>
          </div>
        </div>

      </div>
    );
  }
};