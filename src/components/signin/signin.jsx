import React from 'react';
import api from '../../api';
import dom from 'component-dom';

export default class SignIn extends React.Component {
  constructor() {
    super();
    this.state = {
      errorMessage: null,
      signup: false,
      validating: false
    };
  }

  componentWillUnmount() {
    this.props.fetch();
  }

  goToSignUp() {
    this.setState({
      signup: true,
      validating: false,
      errorMessage: null
    });
  }

  onKeyPress(ev) {
    if (this.state.errorMessage) {
      this.setState({
        errorMessage: null
      });
    }

    if (ev.key == 'Enter') {
      ev.preventDefault();
      if (!this.state.signup)
        this.signIn();
      else if (!this.state.validating)
        this.signUp();
      else
        this.validate();
    }
  }

  signIn() {
    var email = dom('#email').val();
    var password = dom('#password').val();

    if (this.validateEmail(email)) {
      api.signin.signin(email, password)
      .then(user => {
        if (!user.validated)
          return this.setState({signup: true, validating: true});

        this.props.setLogInState();
      })
      .catch(err => {
        this.setState({
          errorMessage: err
        });
      });
    }
  }

  signUp() {
    var email = dom('#email').val();
    var pass1 = dom('#password1').val();
    var pass2 = dom('#password2').val();

    if (this.validateEmail(email) && this.passwordsMatch(pass1, pass2)) {
      api.signup.signup(email, pass1)
      .then(user => {
        this.setState({validating: true});
      })
      .catch(err => {
        this.setState({
          errorMessage: err
        });
      });
    }
  }

  validate() {
    var code = dom('#code').val();

    if (this.validateCode(code)) {
      api.signup.validate(code)
      .then(user => {
        this.props.setLogInState();
      })
      .catch(err => {
        this.setState({
          errorMessage: err
        });
      });
    }
  }

  validateEmail(email) {
    var re = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
    if (re.test(email)) {
      if (email.indexOf('@dartmouth.edu', email.length - '@dartmouth.edu'.length) !== -1) {
        return true;
      }

      else {
        this.setState({
          errorMessage: 'Must be a Dartmouth email address'
        });
        return false;
      }
    }

    else {
      this.setState({
        errorMessage: 'Not a valid e-mail address.'
      });
      return false;
    }
  }

  validateCode(code) {
    return true;
  }

  passwordsMatch(pass1, pass2) {
    if (pass1 != pass2) {
      this.setState({
        errorMessage: 'Passwords do not match.'
      });
      return false;
    }
    return true;
  }

  render() {
    let errorMessageEls, validatingMessage;
    let emailInput, signInInputs, signUpInputs, validatingInputs;
    let signInButtons, signUpButtons, validatingButtons;
    let signInP, validatingP;

    if (this.state.errorMessage) {
      let userNotFound;
      if (this.state.errorMessage == 'User not found') {
        userNotFound = (<a onClick={this.goToSignUp.bind(this)}>Sign Up</a>);
      }

      errorMessageEls = (
        <div className="error-message-container">
          <span>{`${this.state.errorMessage}. `}</span>
          {userNotFound}
        </div>
      );
    }

    /**
     * If signin
     */
    if (!this.state.signup) {
      emailInput = (<input id="email" type="email" placeholder="first.m.last.00@dartmouth.edu" key="email"/>);

      signInInputs = [(<input id="password" type="password" placeholder="•••••••••" key="password"/>)];

      signInButtons = [
        (<div key="signin-button" className="btn signin" onClick={this.signIn.bind(this)}>Sign In</div>),
        (<div key="signup-button" className="btn signup" onClick={this.goToSignUp.bind(this)}>Sign Up</div>)
      ];

      signInP = (<a href="#">Forgot your password?</a>);
    }

    /**
     * If signup
     */
    else if (this.state.signup && !this.state.validating) {
      emailInput = (<input id="email" type="email" placeholder="first.m.last.00@dartmouth.edu" key="email"/>);

      signUpInputs = [
        (<input id="password1" type="password" name="password1" placeholder="Password" key="password1"/>),
        (<input id="password2" type="password" name="password2" placeholder="Re-type password" key="password2"/>)
      ];

      signUpButtons = (<div className="btn signup" onClick={this.signUp.bind(this)}>Sign Up</div>);
    }

    /**
     * If validating
     */
    else {
      validatingInputs = (<input id="code" type="text" name="code" placeholder="00000"/>);

      validatingMessage = (
        <div className="validating-message-container">
          <span>Check your email for a validation code</span>
        </div>
      );

      validatingButtons = (<div className="btn validate" onClick={this.validate.bind(this)}>Validate</div>);

      validatingP = (<a href="#">Resend validation code</a>);
    }


    return (
      <div className="login-container">
        <div className="login-page">

          <h1>Poll U</h1>
          <h2>The first reliable college opinion polling platform for groundhogs</h2>

          <div className="signin">
            <div className="signin-box" onKeyPress={this.onKeyPress.bind(this)}>

              <img src="img/PollU_FGLogo.png" alt="Poll U Logo" width="100"/>
              {errorMessageEls}
              {emailInput}
              {signInInputs}
              {signUpInputs}
              {validatingInputs}
              {validatingMessage}
              <div className="buttons-container">
                {signInButtons}
                {signUpButtons}
                {validatingButtons}
              </div>

            </div>
          </div>
          <p>
            {signInP}
            {validatingP}
          </p>
        </div>

      </div>
    );
  }
}
