import React from 'react';
import Hammer from 'react-hammerjs';
import store from 'store';
import api from '../../api';

/**
 * Sidebar component
 * Expects props:
 *   onLogoClick – toggle whether the sidebar is being shown
 *   setLogInState - sets app.state.login 
 */
export default class Sidebar extends React.Component {
  constructor() {
    super();
    this.state = {
      user: store.get('user')
    };
  }

  componentWillMount() {
    api.user.me()
    .then(user => {
      this.setState({user: user})
      store.set('user', user);
    })
    .catch(err => {
      alert(err);
    });
  }

  signOut() {
    api.signin.signout();
    this.props.toggleSidebar();
    this.props.setLogInState();
  }

  render() {
    let user = this.state.user;

    return (
      <div className="sidebar">
        <div className="name-container">
          <div className="first-name-container">
            <span>{user.firstName}</span>
          </div>

          <div className="last-name-container">
            <span>{user.lastName}</span>
          </div>
        </div>

        <div className="reward-points-container">
          <div className="points-container">
            <div className="points">
              <span>{user.numPoints || 0}</span>
            </div>
          </div>

          <div className="points-text-container">
            <div className="points-text">
              <span>{"REWARD POINTS"}</span>
            </div>
          </div>
        </div>

        <div className="menu-options-container">
          <Hammer onTap={this.props.hideBank} >
            <div className="home menu-opt">
              <div className="menu-opt-img-container">
                <img src="img/home-icon.svg"/>
              </div>
              <div className="menu-opt-text-container">
                <span>{"Home"}</span>
              </div>
            </div>
          </Hammer>

          <div className="profile menu-opt">
            <div className="menu-opt-img-container ">
              <img src="img/profile-icon.svg"/>
            </div>
            <div className="menu-opt-text-container">
              <span>{"Profile"}</span>
            </div>
          </div>

          <div className="completed-surveys menu-opt">
            <div className="menu-opt-img-container">
              <img src="img/completed-surveys-icon.svg"/>
            </div>
            <div className="menu-opt-text-container">
              <span>{"Your Completed Surveys"}</span>
            </div>
          </div>

          <div className="suggest-survey menu-opt">
            <div className="menu-opt-img-container">
              <img src="img/suggest-survey-icon.svg"/>
            </div>
            <div className="menu-opt-text-container">
              <span>{"Suggest a Survey"}</span>
            </div>
          </div>

          <Hammer onTap={this.props.showBank} >
            <div className="bank menu-opt" >
              <div className="menu-opt-img-container">
                <img src="img/bank-icon-green.svg"/>
              </div>
              <div className="menu-opt-text-container">
                <span>{"Bank"}</span>
              </div>
            </div>
          </Hammer>
        </div>


        <div className="sign-out menu-opt" onClick={this.signOut.bind(this)}>
          <div className="menu-opt-img-container">
            <img src="img/signout-icon.svg"/>
          </div>
          <div className="menu-opt-text-container">
            <span>{"Sign Out"}</span>
          </div>
        </div>
      </div>
    );
  }
};
