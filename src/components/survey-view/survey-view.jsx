import React from 'react';
import SurveyCard from '../survey-card/survey-card';
import QBox from '../q-box/q-box';
import ConclusionsView from '../conclusions-view/conclusions-view';
import DataExplorer from '../data-explorer/data-explorer';
import Hammer from 'react-hammerjs';
import api from '../../api';

/**
 * If we're currently looking at a survey, either completed or not
 * 
 */
export default class SurveyView extends React.Component {
  constructor() {
    super();
    this.state = {
      questionIdx: 0,
      data: null,
      answers: [],
      submittingInProgress: false
    };
    this.answersChanged = false;
  }

  componentWillMount() {
    if (!this.state.data) {
      api.survey.one(this.props.active.id)
      .then(survey => {
        this.setState({
          data: survey
        });
      })
      .catch(err => {
        alert(err);
      });
    }
  }

  /**
   * Watches for changes to decide when to submit the answers
   */
  componentDidUpdate() {
    if (this.state.data && this.state.questionIdx == this.state.data.questions.length 
        && !this.state.submittingInProgress && this.answersChanged) {

      this.setState({
        submittingInProgress: true
      });
      this.answersChanged = false;

      api.answer.answer(this.state.answers, this.state.data.id)
      .then(body => {
        this.setState({
          submittingInProgress: false
        });
      })
      .catch(err => {
        alert(err);
      });
    }
  }

  componentWillUnmount() {
    this.props.fetch();
  }


  onQuestionPan(ev) {
    switch (ev.direction) {
      // pan towards the right
      case 2:
        if (this.state.data.complete) {
          this.setState({
            questionIdx: Math.min(this.state.questionIdx + 1, this.state.data.questions.length - 1)
          });
        }

        else {
          this.setState({
            questionIdx: Math.min(this.state.questionIdx + 1, this.state.data.questions.length)
          });
        }
        break;

      // pan towards the left
      case 4:
        this.setState({
          questionIdx: Math.max(this.state.questionIdx - 1, 0)
        });
        break;
    }
  }

  setAnswer(answer) {
    this.answersChanged = true;
    var answerToQ = this.state.answers.filter(a => a.question == answer.question)[0];
    if (answerToQ) {
      var idx = this.state.answers.indexOf(answerToQ);
      this.state.answers.splice(idx, 1, answer);
    } else {
      this.state.answers.push(answer);
    }

    this.forceUpdate();
  }

  render() {
    let panOptions = {
      direction: ['left', 'right'],
      threshold: 20
    };

    let survey = this.state.data;

    let question;
    if (survey)
      question = survey.questions[this.state.questionIdx];

    let currentAnswer;
    if (question) {
      currentAnswer = this.state.answers.filter(a => a.question == question.id)[0];
    }

    if (!currentAnswer) currentAnswer = {selections: []};

    let ifSurvey, main;
    if (survey) {
      if (!survey.complete) {
        main = (
          <Hammer key="uncomplete" onPanEnd={this.onQuestionPan.bind(this)} options={panOptions}>
            <QBox question={question} survey={survey} answer={currentAnswer}
            setAnswer={this.setAnswer.bind(this)}
            submittingInProgress={this.state.submittingInProgress} />
          </Hammer>
        );
      }

      else if (survey.complete) {
        main = [
          (<ConclusionsView key="conclusions-view" conclusions={survey.conclusions} />),
          (<DataExplorer key="data-explorer" question={question} survey={survey} onQuestionPan={this.onQuestionPan.bind(this)} />)
        ];
      }

      ifSurvey = [
        (<SurveyCard survey={survey} key={survey.id} setActive={this.props.setActive} onClose={true} />),
        main
      ];
    }

    return (
      <div className="survey-view">
        {ifSurvey}
      </div>
    );
  }
};
