import React from 'react';
import Hammer from 'react-hammerjs';
import moment from 'moment';

export default class SurveyCard extends React.Component {
  constructor() {
    super();
  }

  onClick() {
    this.props.setActive(this.props.survey);
  }

  onCloseClick(ev) {
    ev.srcEvent.stopPropagation();
    this.props.setActive(this.props.survey.complete ? 'complete' : 'ongoing');
  }

  /**
   * Expects props to have
   *   .survey
   * Options props
   *   .onClose – should be set if being rendered by survey-view
   *              shouldn't be set if rendered by home
   */
  render() {
    let createdAt = moment(this.props.survey.createdAt);
    let resultsAt = moment(this.props.survey.resultsAt);
    let wordCount = this.props.survey.wordCount;
    let surveyDuration = moment.duration(1000*wordCount).humanize();

    let now = moment(Date.now());
    let ago = moment.duration(createdAt.diff(now)).humanize() + ' ago';
    let resultsDuration = moment.duration(resultsAt.diff(now)).humanize();
    let resultsAvailable = 'Results available in';
    if(resultsAt<now){
      resultsAvailable = ""
      resultsDuration = "Results Live";
    }

    let surveyCardStyle = {};
    if (!this.props.onClose)
      surveyCardStyle = {paddingLeft: '10px', paddingRight: '10px'};

    let closeButton;
    let estBox;
    if (this.props.onClose) {
      closeButton = (
        <div className="survey-close-button">
          <Hammer onTap={this.onCloseClick.bind(this)}>
            <img src="img/X.png"/>
          </Hammer>
        </div>
      );
    } else {
      estBox = (
        <div className="survey-estimated-completion-container">
              <div className="estimated-title">
                <span>Estimated Completion Time</span>
              </div>
              <div className="estimated-time">
                <img src="img/white-clock.png"/>
                <span>{surveyDuration}</span>
              </div>
            </div>
        );
    }

    return (
      <Hammer onTap={this.onClick.bind(this)}>
        <div className="survey-card" style={surveyCardStyle}>
          <div className="banner-container" style={{backgroundImage:`url(${this.props.survey.imgSrc})`}} >
            <div className="survey-title-container">
              <span className="survey-title">{this.props.survey.title}</span>
            </div>
            {estBox}
            {closeButton}
          </div>

          <div className="bottom-container">
            <div className="description-container">
              <span>{this.props.survey.description}</span>
            </div>
            <div className="middle-box">
              <div className="resultstitle-container">
                <span>{`${resultsAvailable}`}</span>
              </div>
              <div className="resultsat-container">
                <span>{`${resultsDuration}`}</span>
              </div>
            </div>
            <div className="right-box">
              <div className="pointvalue-container">
                <span>{`+${this.props.survey.pointValue}`}</span>
              </div>
              <div className="date-container">
                <span className="date">{ago}</span>
              </div>
            </div>
          </div>
        </div>
      </Hammer>
    );
  }
};
