import api from '../api';
import React from 'react';
import Sidebar from './sidebar/sidebar';
import Header from './header/header';
import SignIn from './signin/signin';
import Home from './home/home';
import SurveyView from './survey-view/survey-view';
import BankView from './bank-view/bank-view';
import Hammer from 'react-hammerjs';

/**
 * The main thing
 */
export default class App extends React.Component {
  constructor() {
    super();

    this.surveys = [];
    this.user = {};

    this.state = {
      loggedIn: false,
      active: null,
      bankview: false,
      view: {
        w: window.innerWidth,
        h: window.innerHeight
      }
    };

    if (this.state.view.w > 700) {
      this.state.desktopView = true;
      this.state.sidebarShown = true;
    }

    else {
      this.state.desktopView = false;
      this.state.sidebarShown = false;
    }

    window.onresize = this.resize.bind(this);
  }

  resize() {
    this.setState({
      view: {
        w: window.innerWidth,
        h: window.innerHeight
      }
    });
  }

  /**
   * Set login state
   * If we're logged in, let's get some surveys
   */
  componentWillMount() {
    this.state.loggedIn = api.signin.isSignedIn();

    if (this.state.loggedIn)
      this.fetch();     
  }

  /**
   * Get all surveys, reupdate the app
   * fetch will log out the user if they should be
   */
  fetch() {
    api.survey.all()
    .then(surveys => {
      this.surveys = surveys;
      this.forceUpdate();
    })
    .catch(err => {
      alert(err);
      this.setLogInState();
    }); 
  }

  /**
   * Reset the user object - should be called if a change
   * in the number of points or something
   */
  resetUser(user) {
    if (!user) {
      api.user.me()
      .then(user => {
        this.user = user;
        this.forceUpdate();
      })
      .catch(err => {
        alert(err);
        this.setLogInState();
      });    
    } else {
      this.user = user;
      this.forceUpdate();
    }
  
  }

  /**
   * Sets app.state.login to whether the current browser environment
   * indicates that the user is currently logged in
   */
  setLogInState() {
    this.setState({loggedIn: api.signin.isSignedIn()});
  }

  /**
   * Setter for this.state.active
   * If active is a survey, we should be viewing that survey
   * 
   */
  setActive(survey) {
    this.setState({active: survey});
  }

  /**
   * Toggle whether the sidebar is being shown 
   */
  toggleSidebar() {
    if (!this.state.desktopView)
      this.setState({sidebarShown: !this.state.sidebarShown});
  }

  /**
   * Show the bank view
   */
  showBank() {
    this.setState({bankview: true, sidebarShown: false});
  }

  /**
   * Hide the bank view
   */
  hideBank() {
    this.setState({bankview: false, sidebarShown: false});
  }

  render() {
    let methods = {};
    'fetch setLogInState toggleSidebar setActive hideBank showBank resetUser'.split(' ').forEach(m => {
      methods[m] = this[m].bind(this);
    });

    if (!this.state.loggedIn) {
      return (
        <div id="app" className="desktop">
          <div id="main-app">
            <SignIn setLogInState={methods.setLogInState} fetch={methods.fetch} />
          </div>
        </div>
      );
    }

    let sidebarComp;
    if (this.state.sidebarShown)
      sidebarComp = (<Sidebar 
        user={this.user} 
        setLogInState={methods.setLogInState} 
        toggleSidebar={methods.toggleSidebar}
        showBank={methods.showBank}
        hideBank={methods.hideBank} />);

    let header = (<Header numPoints={this.user.numPoints} toggleSidebar={methods.toggleSidebar} />);

    let mainContents;
    if (!this.state.active || (typeof this.state.active == 'string')) {
      mainContents = (
        <Home surveys={this.surveys} active={this.state.active} setActive={methods.setActive} />
      );
    } else {
      mainContents = (
        <SurveyView active={this.state.active} setActive={methods.setActive} fetch={methods.fetch} />
      );
    }

    if (this.state.bankview) {
      mainContents = (
        <BankView user={this.user} resetUser={methods.resetUser} />
      );
    }

    let overlay;
    if (!this.state.desktopView && this.state.sidebarShown) {
      overlay = (
        <Hammer onTap={methods.toggleSidebar}>
          <div className="overlay-div"></div>
        </Hammer>
      );
    }

    return (
      <div id="app" className={this.state.desktopView ? 'desktop' : ''}>
        {sidebarComp}
        <div id="main-app">
          {header}
          {mainContents}
        </div>
        {overlay}
      </div>
    );
  }
};
