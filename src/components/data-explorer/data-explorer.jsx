import React from 'react';
import Hammer from 'react-hammerjs';
import ProgressBar from '../progress-bar/progress-bar';
import api from '../../api';
import FiltersView from '../filters-view/filters-view';
import DemographicsFilter from '../../lib/demographics-filter';
import aggregate from '../../lib/aggregate';
import numerify from '../../lib/numerify';
import closest from 'component-closest';
import dom from 'component-dom';
import standardDeviation from 'standard-deviation';
import { BarChart } from 'react-d3';


export default class QBox extends React.Component {
  constructor() {
    super();
    this.state = {
      collapsed: true,
      filters: null,
      viewGraph: false,
    };
    this.DemographicsFilter = DemographicsFilter;
  }

  componentWillUnmount() {
    this.DemographicsFilter.clear();
  }

  onHeaderClick(ev) {
    var clickWasOnX = dom(closest(ev.target, 'div', true)).hasClass('data-explorer-x-out');

    if (this.state.collapsed && !clickWasOnX) {
      this.setState({
        collapsed: false
      });
    }

    if (!this.state.collapsed && clickWasOnX) {
      ev.srcEvent.stopPropagation();
      this.setState({
        collapsed: true
      });
    }
  }

  onGraphClick(ev) {
    this.setState({
      viewGraph: true
    })
  }

  onPercentClick(ev) {
    this.setState({
      viewGraph: false
    })
  }

  onFiltersChange() {
    this.forceUpdate();
  }

  render() {
    let filteredAnswers = this.DemographicsFilter.filter(this.props.question.answers);

    let aggregated = aggregate(filteredAnswers);
    let stddev = standardDeviation(numerify(aggregated.selections, this.props.question.options.map(o => o.value)));
    if (stddev == null)
      stddev = 'N/A';

    let N = filteredAnswers.length;

    this.props.question.options.forEach(o => {
      if (aggregated.values[o.value])
        o.percent = aggregated.values[o.value] / aggregated.selections.length * 100;
      else
        o.percent = 0;
    });

    let qNum = this.props.survey.questions.indexOf(this.props.question);
    let headerClass = this.state.collapsed ? 'colapsed' : 'expanded';

    /**
     * Question container exists if we're not collapsed
     * X Out exists if state is not collapsed
     * Filters View exists if state is not collapsed
     */
    let xout, questionContainer, filtersView, options, graph, barData;

    if (!this.state.collapsed) {
      xout = (
        <Hammer onTap={this.onHeaderClick.bind(this)}>
          <div className="data-explorer-x-out">
            <img src="img/X.png"/>
          </div>
        </Hammer>
      );

      if (! this.state.viewGraph) {

        options = this.props.question.options.map(o => {
          return (
            <div className="option" key={o.value} data={o.value}>
              <div className="option-box">
                <span>{`${o.percent}%`}</span>
              </div>

              <div className="option-text">
                <div className="option-text-container">
                  <span data={o.value}>{o.text}</span>
                </div>
              </div>
            </div>
          );
        });
      } else {
        
        // array of dictionaries (format needed for d3)
        var values = [];
        // map all question/value pair into a dictionary in values
        this.props.question.options.map(o => {
          values.push({ "x": o.text, "y": o.percent});

        })

        // data for bar graph
        barData = [
        {
          "name": "Series A",
          "values": values
        }];
        
        // create BarChart
        graph = (<div className="graph">
          <BarChart
                  data={barData}
                  width={300}
                  height={150}
                  fill={'#3182bd'}
                  title='Results'
                  xAxisTickValues={[]}
                  yAxisLabel="%"
          />
          </div>);
      }


      questionContainer = (
        <Hammer onPanEnd={this.props.onQuestionPan} options={null}>
          <div className="question-container">

            <ProgressBar 
              max={this.props.survey.questions.length} 
              current={qNum}
              name="data-explorer" />

            <div className="question-header">

              <div className="number-container">
                <div className="number">
                  <span>{`Q${qNum}`}</span>
                </div>

                <div className="out-of">
                  <span>{`of ${this.props.survey.questions.length}`}</span>
                </div>
              </div>

              <div className="question-text-container">
                <div className="question-text-container">
                  <span>{this.props.question.description}</span>
                </div>
              </div>

            </div>

            <div className="results-container">
              <div className="stats-container">
                <div className="stddev-container">
                  <span>σ<sup>2</sup>{`: ${stddev}`}</span>
                </div>
                <div className="n-container">
                  <span>{`N: ${N}`}</span>
                </div>
              </div>

              <div className="numbers-container">
                <div className="options-container">
                  {options}
                  {graph}
          
                </div>
                

                <div className="buttons-container">
                  <div className="graph-view-button">
                    <Hammer onTap={this.onPercentClick.bind(this)}>
                      <img src="img/home-icon.svg"/>
                    </Hammer>
                  </div>
                  <div className="percent-view-button">
                    <Hammer onTap={this.onGraphClick.bind(this)}>
                      <img src="img/profile-icon.svg"/>
                    </Hammer>
                  </div>

                </div>

              </div>
            </div>
          </div>
        </Hammer>
      );

      filtersView = (
        <FiltersView DemographicsFilter={DemographicsFilter} onFiltersChange={this.onFiltersChange.bind(this)}/>
      );
    }

    return (
      <div className="data-explorer-container">
        <Hammer onTap={this.onHeaderClick.bind(this)}>
          <div className={`data-explorer-header ${headerClass}`}>
            <div className="data-explorer-header-text">
              <span>Explore the Data</span>
            </div>

            {xout}
          </div>
        </Hammer>

        {questionContainer}
        {filtersView}
      </div>
    );
  }
};
