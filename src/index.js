import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import TouchEmulator from 'hammer-touchemulator';

/**
 * Initialize the app, 
 */
const app = {
  /**
   * Application Constructor
   */
  initialize: function() {
    this.bindEvents();
  },

  /**
   * Phonegap stuff, get's it all going
   * https://cordova.apache.org/docs/en/4.0.0/cordova/events/events.deviceready.html
   */
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },

  /**
   * Phonegap call
   * @return {[type]} [description]
   */
  
  onDeviceReady: function() {
    ReactDOM.render(React.createElement(App), document.querySelector('#reactAppContainer'));
    
    /**
     * Sometimes necessary to be able to test touch events in browser, I'm not sure when
     * It depends on whether Chrome's 
     * SHOULD BE NOT USED IN PRODUCTION
     */
    
    // TouchEmulator();
  }
};

app.initialize();
