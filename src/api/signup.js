import request from './request';
import store from 'store';

exports.signup = function (email, password) {
  return new Promise((resolve, reject) => {
    request
    .post('/api/signup')
    .send({email: email, password: password})
    .end((err, res) => {
      if (err) return reject(err);

      store.set('user', res.body.user);
      return resolve(res.user);
    });
  });
};

exports.requestNewCode = function (email, password) {
  return new Promise((resolve, reject) => {
    request
    .post('/api/signup/resend')
    .send({email: email, password: password})
    .end((err, res) => {
      if (err) return reject(err);

      store.set('user', res.body.user);
      return resolve(res.user);
    });
  });
};

exports.validate = function (code) {
  var email = store.get('user').email;

  return new Promise((resolve, reject) => {
    request
    .post('/api/signup/validate')
    .send({email: email, code: code})
    .end((err, res) => {
      if (err) return reject(err);

      store.set('token', res.body.token);
      store.set('user', res.body.user);
      return resolve(res.user);
    });
  });
};