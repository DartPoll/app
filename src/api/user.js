import request from './request';
import store from 'store';

exports.me = function () {
  return new Promise((resolve, reject) => {
    request
    .get('/api/user/me')
    .end((err, res) => {
      if (err) return reject(err);

      store.set('user', res.body.user);
      return resolve(res.body.user);
    });
  });
};