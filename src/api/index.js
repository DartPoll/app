exports.answer = require('./answer');
exports.survey = require('./survey');
exports.signin = require('./signin');
exports.signup = require('./signup');
exports.user = require('./user');
exports.prize = require('./prize');