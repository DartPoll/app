import request from './request';
import models from '../models';
import store from 'store';

exports.answer = function (answers, surveyId) {
  return new Promise((resolve, reject) => {
    var errors = [];
    answers.forEach(a => {
      errors = models.answer.validateAnswer(a, errors);
    });

    if (errors.length > 0) return reject(errors);

    request
    .post(`/api/survey/${surveyId}/answer`)
    .send({answers})
    .end((err, res) => {
      if (err) return reject(err);

      store.set('user', res.body.user);
      return resolve(res.body);
    });
  });
};
