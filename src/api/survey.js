import request from './request';

exports.all = function () {
  return new Promise((resolve, reject) => {
    request
    .get('/api/survey/all')
    .end((err, res) => {
      if (err) return reject(err);

      resolve(res.body);
    });
  });
};

exports.one = function (id) {
  return new Promise((resolve, reject) => {
    request
    .get(`/api/survey/${id}`)
    .end((err, res) => {
      if (err) return reject(err);

      resolve(res.body);
    });
  });
};