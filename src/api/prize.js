import request from './request';
import store from 'store';

exports.all = function () {
  return new Promise((resolve, reject) => {
    request
    .get('/api/prize/all')
    .end((err, res) => {
      if (err) return reject(err);      
      if (res.body.error) return reject(res.body.error);

      return resolve(res.body);
    });
  });
};

exports.claim = function (prizeId) {
  return new Promise((resolve, reject) => {
    request
    .post(`/api/prize/claim/${prizeId}`)
    .end((err, res) => {
      if (err) return reject(err);
      if (res.body.error) return reject(res.body.error);

      store.set('user', res.body.user);
      return resolve(res.body.user);
    });
  });
};
