import request from './request';
import store from 'store';

exports.signin = function (email, password) {
  return new Promise((resolve, reject) => {
    if (store.get('token')) {
      return resolve(store.get('user'));
    }

    request
    .post('/api/signin')
    .send({email: email, password: password})
    .end((err, res) => {
      if (err) return reject(err);
      
      if (res.body.error) return reject(res.body.error);

      store.set('token', res.body.token);
      store.set('user', res.body.user);

      return resolve(res.body.user);
    });
  });
};

exports.signout = function () {
  store.remove('token');
  store.remove('user');
};

exports.isSignedIn = function () {
  var token = store.get('token');
  var user = store.get('user');
  return (token && user && (token != 'undefined') && (user != 'undefined'));
};