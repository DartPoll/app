var gulp = require('gulp');
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');

/**
 * Move ./src/img/*.png to ./www/img/*.png, ignored unchanged files and minimizing them
 */
gulp.task('images', function() {
  var dest = './www/img';

	return gulp.src('./src/img/**')
		.pipe(changed(dest))
		.pipe(imagemin())
		.pipe(gulp.dest(dest));
});
