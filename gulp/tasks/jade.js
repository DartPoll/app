var gulp = require('gulp');
var jade = require('gulp-jade');
var changed = require('gulp-changed');

var dest = './www';

/**
 * Compile ./src/index.jade to ./www/index.html
 */
gulp.task('jade', function() {
	return gulp.src('./src/index.jade')
    .pipe(jade())
		.pipe(changed(dest))
		.pipe(gulp.dest(dest));
});