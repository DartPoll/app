var gulp = require('gulp');
var gutil = require('gulp-util');

gulp.task('watch', function() {
	gulp.watch('./src/**/*.js', ['browserify']);
  gulp.watch('./src/**/*.jsx', ['browserify']);
	gulp.watch('./src/**/*.png', ['images']);
	gulp.watch('./src/**/*.styl', ['styles']);
	gulp.watch('./src/index.jade', ['jade']);
});