var gulp = require('gulp');
var gutil = require('gulp-util');
var browserify = require('browserify');
var babelify = require('babelify');
var jade = require('react-jade');
var handleErrors = require('../util/handleErrors');
var source = require('vinyl-source-stream');

gulp.task('browserify', function() {
  return browserify({
    entries: ['./src/index.js'],
    extensions: ['.js', '.jsx'],
    paths: ['./node_modules','./src/js/'],
    debug: true
  })
  .transform(babelify, {presets: ['es2015', 'react']})
  .transform(jade)
  .bundle()
  .on('error', handleErrors)
  .pipe(source('./src/index.js'))
  .pipe(gulp.dest('./www'));
});