var gulp = require('gulp');
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');

/**
 * Move ./src/fonts/*.ttf to ./www/fonts/*.ttf, ignored unchanged files 
 */
gulp.task('fonts', function() {
  var dest = './www/fonts';

  return gulp.src('./src/fonts/**')
    .pipe(changed(dest))
    .pipe(gulp.dest(dest));
});
